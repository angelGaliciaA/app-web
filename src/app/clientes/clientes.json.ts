import { Cliente } from './cliente';

  export const CLIENTES : Cliente[] = [
{id: 1, nombre: 'Antonio', apellido: 'Guzmán', email: 'profesor@bolsadeideas.com', createAt: '2018-11-30'},
{id: 2, nombre: 'Angelote', apellido: 'Guzmán', email: 'profesor@bolsadeideas.com', createAt: '2018-11-30'},
{id: 3, nombre: 'Antonio', apellido: 'Guzmán', email: 'profesor@bolsadeideas.com', createAt: '2018-11-30'},
{id: 4, nombre: 'Angelius', apellido: 'Guzmán', email: 'profesor@bolsadeideas.com', createAt: '2018-11-30'},
{id: 5, nombre: 'Antonio', apellido: 'Guzmán', email: 'profesor@bolsadeideas.com', createAt: '2018-11-30'},
{id: 6, nombre: 'Angeliux', apellido: 'Guzmán', email: 'profesor@bolsadeideas.com', createAt: '2018-11-30'},
{id: 7, nombre: 'Antonio', apellido: 'Guzmán', email: 'profesor@bolsadeideas.com', createAt: '2018-11-30'},
{id: 8, nombre: 'Angelo', apellido: 'Guzmán', email: 'profesor@bolsadeideas.com', createAt: '2018-11-30'},
{id: 9, nombre: 'Antonio', apellido: 'Guzmán', email: 'profesor@bolsadeideas.com', createAt: '2018-11-30'},
{id: 10, nombre: 'Angelius59', apellido: 'Guzmán', email: 'profesor@bolsadeideas.com', createAt: '2018-11-30'}
];
